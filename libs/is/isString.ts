export function isString(object: any): object is string {
	return Object.prototype.toString.call(object) === '[object String]'
}
