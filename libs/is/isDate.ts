export function isDate(object: any): object is Date {
	return Object.prototype.toString.call(object) === '[object Date]'
}
