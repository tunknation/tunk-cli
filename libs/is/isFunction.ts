export function isFunction(object: any): object is Function {
	return Object.prototype.toString.call(object) === '[object Function]'
}
