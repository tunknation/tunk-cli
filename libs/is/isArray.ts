export function isArray(object: any): object is any[] {
	return Object.prototype.toString.call(object) === '[object Array]'
}
