export function isSymbol(object: any): object is symbol {
	return Object.prototype.toString.call(object) === '[object Symbol]'
}
