export function isBoolean(object: any): object is boolean {
	return Object.prototype.toString.call(object) === '[object Boolean]'
}
