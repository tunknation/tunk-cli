import { isNull } from './isNull'
import { isUndefined } from './isUndefined'

export function isNullOrUndefined(object: any): object is null | undefined {
	return isNull(object) || isUndefined(object)
}
