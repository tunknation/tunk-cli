import { isBoolean } from './isBoolean'
import { isNull } from './isNull'
import { isUndefined } from './isUndefined'
import { isNumber } from './isNumber'
import { isString } from './isString'
import { isSymbol } from './isSymbol'

export function isPrimitive(
	object: any
): object is boolean | null | undefined | number | string | symbol {
	return (
		isBoolean(object) ||
		isNull(object) ||
		isUndefined(object) ||
		isNumber(object) ||
		isString(object) ||
		isSymbol(object)
	)
}
