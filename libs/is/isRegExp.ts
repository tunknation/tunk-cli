export function isRegExp(object: any): object is RegExp {
	return Object.prototype.toString.call(object) === '[object RegExp]'
}
