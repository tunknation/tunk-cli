import { isDate } from './isDate'
import { isRegExp } from './isRegExp'
import { isSymbol } from './isSymbol'

export function isSpecial(object: any): object is Date | RegExp | symbol {
	return isDate(object) || isRegExp(object) || isSymbol(object)
}
