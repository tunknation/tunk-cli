import { isObject } from './isObject'
import { isString } from './isString'

export function isJSON(object: any, passObject?: boolean): boolean {
	if (passObject && isObject(object)) return true

	if (!isString(object)) return false

	const test = object.replace(/\s/g, '').replace(/\n|\r/, '')

	if (/^\{(.*?)\}$/.test(test)) return /"(.*?)":(.*?)/g.test(test)

	if (/^\[(.*?)\]$/.test(test)) {
		return test
			.replace(/^\[/, '')
			.replace(/\]$/, '')
			.replace(/},{/g, '}\n{')
			.split(/\n/)
			.map((s: any) => {
				return isJSON(s)
			})
			.reduce((_prev: any, curr: boolean) => {
				return !!curr
			})
	}

	return false
}
