export function isNull(object: any): object is null {
	return Object.prototype.toString.call(object) === '[object Null]'
}
