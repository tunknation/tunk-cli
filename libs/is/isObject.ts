export function isObject(object: any): object is Record<string, any> {
	return Object.prototype.toString.call(object) === '[object Object]'
}
