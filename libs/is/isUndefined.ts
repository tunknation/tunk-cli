export function isUndefined(object: any): object is undefined {
	return Object.prototype.toString.call(object) === '[object Undefined]'
}
