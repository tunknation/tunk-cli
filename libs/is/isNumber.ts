export function isNumber(object: any): object is number {
	return Object.prototype.toString.call(object) === '[object Number]'
}
