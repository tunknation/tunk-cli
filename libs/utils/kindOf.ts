export function kindOf(thing: undefined): 'undefined'
export function kindOf(thing: null): 'null'
export function kindOf(thing: boolean): 'boolean'
export function kindOf(thing: Buffer): 'buffer'
export function kindOf(thing: number): 'number'
export function kindOf(thing: string): 'string'
export function kindOf(thing: IArguments): 'arguments'
export function kindOf(thing: Date): 'date'
export function kindOf(thing: any[]): 'array'
export function kindOf(thing: RegExp): 'regexp'
export function kindOf(thing: Error): 'error'
export function kindOf(
	thing: Iterator<any>
): 'generator' | 'stringiterator' | 'arrayiterator' | 'mapiterator' | 'setiterator'
export function kindOf(thing: (...object: any[]) => any): 'function' | 'generatorfunction'
export function kindOf(thing: symbol): 'symbol'
export function kindOf(thing: Promise<any>): 'promise'
export function kindOf(thing: Map<any, any>): 'map'
export function kindOf(thing: WeakMap<any, any>): 'weakmap'
export function kindOf(thing: Set<any>): 'set'
export function kindOf(thing: WeakSet<any>): 'weakset'
export function kindOf(thing: Int8Array): 'int8array'
export function kindOf(thing: Uint8Array): 'uint8array'
export function kindOf(thing: Uint8ClampedArray): 'uint8clampedarray'
export function kindOf(thing: Int16Array): 'int16array'
export function kindOf(thing: Uint16Array): 'uint16array'
export function kindOf(thing: Int32Array): 'int32array'
export function kindOf(thing: Uint32Array): 'uint32array'
export function kindOf(thing: Float32Array): 'float32array'
export function kindOf(thing: Float64Array): 'float64array'
export function kindOf(thing: any): string
export function kindOf(thing: any): string {
	if (thing === undefined) return 'undefined'
	if (thing === null) return 'null'

	const type = typeof thing
	if (type === 'boolean') return 'boolean'
	if (type === 'string') return 'string'
	if (type === 'number') return 'number'
	if (type === 'symbol') return 'symbol'
	if (type === 'function') {
		return isGeneratorFn(thing) ? 'generatorfunction' : 'function'
	}

	if (isArray(thing)) return 'array'
	if (isBuffer(thing)) return 'buffer'
	if (isArguments(thing)) return 'arguments'
	if (isDate(thing)) return 'date'
	if (isError(thing)) return 'error'
	if (isRegexp(thing)) return 'regexp'

	switch (ctorName(thing)) {
		case 'Symbol':
			return 'symbol'
		case 'Promise':
			return 'promise'

		// Set, Map, WeakSet, WeakMap
		case 'WeakMap':
			return 'weakmap'
		case 'WeakSet':
			return 'weakset'
		case 'Map':
			return 'map'
		case 'Set':
			return 'set'

		// 8-bit typed arrays
		case 'Int8Array':
			return 'int8array'
		case 'Uint8Array':
			return 'uint8array'
		case 'Uint8ClampedArray':
			return 'uint8clampedarray'

		// 16-bit typed arrays
		case 'Int16Array':
			return 'int16array'
		case 'Uint16Array':
			return 'uint16array'

		// 32-bit typed arrays
		case 'Int32Array':
			return 'int32array'
		case 'Uint32Array':
			return 'uint32array'
		case 'Float32Array':
			return 'float32array'
		case 'Float64Array':
			return 'float64array'
		default:
			break
	}

	if (isGeneratorObj(thing)) {
		return 'generator'
	}

	// Non-plain objects
	const nonPlainType = Object.prototype.toString.call(thing)
	switch (nonPlainType) {
		case '[object Object]':
			return 'object'
		// iterators
		case '[object Map Iterator]':
			return 'mapiterator'
		case '[object Set Iterator]':
			return 'setiterator'
		case '[object String Iterator]':
			return 'stringiterator'
		case '[object Array Iterator]':
			return 'arrayiterator'
		default:
			break
	}

	// other
	const otherType = Object.prototype.toString.call(thing)
	return otherType.slice(8, -1).toLowerCase().replace(/\s/g, '')
}

function ctorName(thing: any): string | null {
	return thing.constructor ? thing.constructor.name : null
}

function isArray(thing: any): boolean {
	if (Array.isArray) return Array.isArray(thing)
	return thing instanceof Array
}

function isError(thing: any): boolean {
	return (
		thing instanceof Error ||
		(typeof thing.message === 'string' &&
			thing.constructor &&
			typeof thing.constructor.stackTraceLimit === 'number')
	)
}

function isDate(thing: any): boolean {
	if (thing instanceof Date) return true
	return (
		typeof thing.toDateString === 'function' &&
		typeof thing.getDate === 'function' &&
		typeof thing.setDate === 'function'
	)
}

function isRegexp(thing: any): boolean {
	if (thing instanceof RegExp) return true
	return (
		typeof thing.flags === 'string' &&
		typeof thing.ignoreCase === 'boolean' &&
		typeof thing.multiline === 'boolean' &&
		typeof thing.global === 'boolean'
	)
}

function isGeneratorFn(thing: any): boolean {
	return ctorName(thing) === 'GeneratorFunction'
}

function isGeneratorObj(thing: any): boolean {
	return (
		typeof thing.throw === 'function' &&
		typeof thing.return === 'function' &&
		typeof thing.next === 'function'
	)
}

function isArguments(thing: any): boolean {
	try {
		if (typeof thing.length === 'number' && typeof thing.callee === 'function') {
			return true
		}
	} catch (err) {
		if (err.message.indexOf('callee') !== -1) {
			return true
		}
	}
	return false
}

function isBuffer(thing: any): boolean {
	if (thing.constructor && typeof thing.constructor.isBuffer === 'function') {
		return thing.constructor.isBuffer(thing)
	}
	return false
}
