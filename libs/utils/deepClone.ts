import { clone } from './clone'
import { kindOf } from './kindOf'

function isObject(object: any): object is Record<string, any> {
	return Object.prototype.toString.call(object) === '[object Object]'
}

function _cloneObjectDeep(object: any, instanceClone: any): any {
	if (typeof instanceClone === 'function') {
		return instanceClone(object)
	}
	if (instanceClone || isObject(object)) {
		const res = new object.constructor()
		for (const key in object) {
			if (Object.prototype.hasOwnProperty.call(object, key)) {
				res[key] = deepClone(object[key], instanceClone)
			}
		}
		return res
	}
	return object
}

function _cloneArrayDeep(object: any, instanceClone: any): any {
	const res = new object.constructor(object.length)
	for (let i = 0; i < object.length; i += 1) {
		res[i] = deepClone(object[i], instanceClone)
	}

	return res
}

export function deepClone<T>(object: T, instanceClone?: boolean | ((object: T) => T)): T
export function deepClone(
	object: any,
	instanceClone?: boolean | ((object: any) => any)
): any {
	switch (kindOf(object)) {
		case 'object':
			return _cloneObjectDeep(object, instanceClone)
		case 'array':
			return _cloneArrayDeep(object, instanceClone)
		default: {
			return clone(object)
		}
	}
}
