import { kindOf } from './kindOf'

function cloneRegExp(object: any): any {
	const flags =
		object.flags !== undefined ? object.flags : /\w+$/.exec(object) || undefined
	const re = new object.constructor(object.source, flags)
	re.lastIndex = object.lastIndex
	return re
}

function cloneArrayBuffer(object: any): any {
	const res = new object.constructor(object.byteLength)
	new Uint8Array(res).set(new Uint8Array(object))
	return res
}

function cloneTypedArray(object: any): any {
	return new object.constructor(object.buffer, object.byteOffset, object.length)
}

function cloneBuffer(object: any): any {
	const len = object.length
	const buf = Buffer.allocUnsafe ? Buffer.allocUnsafe(len) : Buffer.from(len)
	object.copy(buf)
	return buf
}

function cloneSymbol(object: any): any {
	return Symbol.prototype.valueOf ? Object(Symbol.prototype.valueOf.call(object)) : {}
}

export function clone<T>(object: T): T
export function clone(object: any): any {
	switch (kindOf(object)) {
		case 'array':
			return object.slice()
		case 'object':
			return { ...object }
		case 'date':
			return new Date(object)
		case 'map':
			return new Map(object)
		case 'set':
			return new Set(object)
		case 'buffer':
			return cloneBuffer(object)
		case 'symbol':
			return cloneSymbol(object)
		case 'arraybuffer':
			return cloneArrayBuffer(object)
		case 'float32array':
		case 'float64array':
		case 'int16array':
		case 'int32array':
		case 'int8array':
		case 'uint16array':
		case 'uint32array':
		case 'uint8clampedarray':
		case 'uint8array':
			return cloneTypedArray(object)
		case 'regexp':
			return cloneRegExp(object)
		case 'error':
			return Object.create(object)
		default: {
			return object
		}
	}
}
