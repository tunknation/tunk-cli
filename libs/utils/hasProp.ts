import { isArray, isNumber, isNullOrUndefined } from '../is'

export function hasProp<T>(root: T, param?: keyof any): param is keyof T
export function hasProp<T>(root: T[], param?: number): boolean
export function hasProp(root: any[] | Record<string, any>, param?: keyof any): boolean {
	if (isNullOrUndefined(param)) {
		return false
	}

	if (isArray(root) && isNumber(param)) {
		return Math.abs(param < 0 ? param + 1 : param) < root.length
	}

	return param in root
}
