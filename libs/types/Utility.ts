export type nil = null | undefined

export type Nilable<T> = T | nil
export type Nullable<T> = T | null
export type Undefinable<T> = T | undefined

export type Maybe<T> = T | nil
export type MaybeNull<T> = T | null
export type MaybeUndefined<T> = T | undefined
export type MaybePromise<T> = T | Promise<T>
export type MaybeArray<T> = T | T[]
export type MaybeAsReturnType<T> = T | ((...args: any) => T)

export type AnyParamConstructor<T = any> = new (...args: any[]) => T
export type AnyFunction<T = any> = (...args: any[]) => T
export interface RecursiveArray<T> extends Array<RecursiveArray<T> | T> {}

/**
 * Make selected properties in T optional
 */
export type Optional<T, K extends keyof T> = {
	[P in keyof Omit<T, K>]: T[P]
} &
	{
		[P in K]+?: T[P]
	}

/**
 * Make selected properties in T required
 */
export type Require<T, K extends keyof T> = {
	[P in keyof Omit<T, K>]: T[P]
} &
	{
		[P in K]-?: T[P]
	}

/**
 * Combine every type in array
 */
export type Union<U> = (U extends any
	? (k: U) => void
	: never) extends (k: infer I) => void
	? I
	: never

/**
 * Conditional checking
 */
type IfEquals<X, Y, A = X, B = never> = (<T>() => T extends X ? 1 : 2) extends <
	T
>() => T extends Y ? 1 : 2
	? A
	: B

/**
 * Extract writable keys
 */
export type WritableKeys<T> = {
	[P in keyof T]-?: IfEquals<{ [Q in P]: T[P] }, { -readonly [Q in P]: T[P] }, P>
}[keyof T]

/**
 * Extract readonly keys
 */
export type ReadonlyKeys<T> = {
	[P in keyof T]-?: IfEquals<{ [Q in P]: T[P] }, { -readonly [Q in P]: T[P] }, never, P>
}[keyof T]

/**
 * Extract function keys
 */
export type FunctionsKeys<T> = {
	[P in keyof T]-?: IfEquals<
		{ [Q in P]: T[P] },
		{ [Q in P]: T[P] extends Function ? never : T[P] },
		never,
		P
	>
}[keyof T]

/**
 * Allows for nested objects and arrays to be partial
 */
export type DeepPartial<T> = {
	[P in keyof T]?: T[P] extends (infer U)[]
		? DeepPartial<U>[]
		: T[P] extends object
		? DeepPartial<T[P]>
		: T[P]
}

/**
 * Create enum from object keys
 */
export type Enum<T = object> = Record<keyof T, number | string> & {
	[k: number]: string
}
