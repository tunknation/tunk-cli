import { clone } from '../../../libs/utils/clone'

describe('clone()', (): void => {
	describe('objects', (): void => {
		it('should shallow clone an array of primitives', (): void => {
			expect(clone(['alpha', 'beta', 'gamma'])).toEqual(['alpha', 'beta', 'gamma'])
		})

		it('should shallow clone an array with varied elements', (): void => {
			const source = [0, 'a', {}, [{}], [function() {}], function() {}]
			const result = clone(source)

			expect(result).toEqual(source)
		})

		it('should clone Map', (): void => {
			const source = new Map([[1, 5]])
			const result = clone(source)

			source.set(2, 4)

			expect(source).not.toEqual(result)
		})

		it('should clone Set', (): void => {
			const source = new Set([2, 1, 3])
			const result = clone(source)

			source.add(8)

			expect(source).not.toEqual(result)
		})

		it('should shallow clone arrays', (): void => {
			const source = [1, 2, 3]
			const result = clone(source)

			expect(result).not.toBe(source)
			expect(result).toEqual(source)
		})

		it('should shallow clone a regex with flags', (): void => {
			const source = /foo/g
			const result = clone(source)

			expect(result).not.toBe(source)
			expect(result).toEqual(source)
		})

		it('should shallow clone a regex without any flags', (): void => {
			const source = /foo/
			const result = clone(source)

			expect(result).not.toBe(source)
			expect(result).toEqual(source)
		})

		it('should shallow clone a date', (): void => {
			const source = new Date()
			const result = clone(source)

			expect(result).not.toBe(source)
			expect(result).toEqual(source)
		})

		it('should shallow clone objects', (): void => {
			const source = { a: 1, b: 2, c: 3 }
			const result = clone(source)

			expect(result).not.toBe(source)
			expect(result).toEqual(source)
		})

		it('should shallow clone an array of objects.', (): void => {
			const source = [{ a: 0 }, { b: 1 }]
			const result = clone(source)

			expect(result).not.toBe(source)
			expect(result).toEqual(source)
			expect(result[0]).toEqual(source[0])
		})
	})

	describe('primitives', (): void => {
		it('should return primitives', (): void => {
			expect(clone(0)).toBe(0)
			expect(clone(1)).toBe(1)
			expect(clone('foo')).toBe('foo')
		})

		it('should clone symbols', (): void => {
			const source = { prop: Symbol('prop') }
			const result = clone(source)

			expect(typeof result.prop).toBe('symbol')
			expect(result).not.toBe(source)
			expect(result.prop).toBe(source.prop)
		})
	})
})
