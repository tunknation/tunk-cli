import { kindOf } from '../../../libs/utils/kindOf'

describe('kindOf()', (): void => {
	describe('null and undefined', (): void => {
		it('should work for undefined', (): void => {
			expect(kindOf(undefined)).toBe('undefined')
		})

		it('should work for null', (): void => {
			expect(kindOf(null)).toBe('null')
		})
	})

	describe('primitives', (): void => {
		it('should work for booleans', (): void => {
			expect(kindOf(true)).toBe('boolean')
			expect(kindOf(false)).toBe('boolean')
		})

		it('should work for numbers', (): void => {
			expect(kindOf(42)).toBe('number')
		})

		it('should work for strings', (): void => {
			expect(kindOf('str')).toBe('string')
		})
	})

	describe('objects', (): void => {
		it('should work for arguments', (): void => {
			;(function() {
				// eslint-disable-next-line
				expect(kindOf(arguments)).toBe('arguments')
			})()
		})

		it('should work for buffers', (): void => {
			expect(kindOf(Buffer.allocUnsafe(42))).toBe('buffer')
		})

		it('should work for objects', (): void => {
			class Test {}
			const instance = new Test()
			const literal = {}
			const createdNull = Object.create(null)
			const createdObj = Object.create({})

			expect(kindOf(instance)).toBe('object')
			expect(kindOf(literal)).toBe('object')
			expect(kindOf(createdNull)).toBe('object')
			expect(kindOf(createdObj)).toBe('object')
		})

		it('should work for dates', (): void => {
			expect(kindOf(new Date())).toBe('date')
		})

		it('should work for arrays', (): void => {
			/* eslint no-array-constructor: 0 */
			expect(kindOf([])).toBe('array')
			expect(kindOf([1, 2, 3])).toBe('array')
			expect(kindOf([])).toBe('array')
		})

		it('should work for regular expressions', (): void => {
			expect(kindOf(/./)).toBe('regexp')
			expect(kindOf(new RegExp('^foo$'))).toBe('regexp')
		})

		it('should work for functions', (): void => {
			/* eslint no-new-func: 0 */
			expect(kindOf((): void => {})).toBe('function')
			expect(kindOf(new Function())).toBe('function')
		})

		it('should work for Errors', (): void => {
			expect(kindOf(new Error(''))).toBe('error')
		})
	})

	describe('es6 features', (): void => {
		it('should work for resolved promises', (): void => {
			const promise = Promise.resolve(42)
			expect(kindOf(promise)).toStrictEqual('promise')
		})

		it('should work for rejected promises', (): void => {
			const promise = Promise.reject(new Error('foo bar'))
			promise.catch((): void => {})
			expect(kindOf(promise)).toStrictEqual('promise')
		})

		it('should work for generator functions', (): void => {
			const gen = function* named(): IterableIterator<boolean> {
				return yield true
			}
			expect(kindOf(gen)).toBe('generatorfunction')
		})

		it('should work for generator objects', (): void => {
			const gen = function* named(): IterableIterator<boolean> {
				return yield true
			}
			expect(kindOf(gen())).toBe('generator')
		})

		it('should work for template strings', (): void => {
			const name = 'Foo'
			expect(kindOf(`Welcome ${name} buddy`)).toBe('string')
		})

		it('should work for Map', (): void => {
			const map = new Map()
			expect(kindOf(map)).toBe('map')
			expect(kindOf(map.set)).toBe('function')
			expect(kindOf(map.get)).toBe('function')
		})

		it('should work for WeakMap', (): void => {
			const weakmap = new WeakMap()
			expect(kindOf(weakmap)).toBe('weakmap')
			expect(kindOf(weakmap.set)).toBe('function')
			expect(kindOf(weakmap.get)).toBe('function')
		})

		it('should work for Set', (): void => {
			const set = new Set()
			expect(kindOf(set)).toBe('set')
			expect(kindOf(set.add)).toBe('function')
		})

		it('should work for WeakSet', (): void => {
			const weakset = new WeakSet()
			expect(kindOf(weakset)).toBe('weakset')
			expect(kindOf(weakset.add)).toBe('function')
		})

		it('should work for Set Iterator', (): void => {
			const SetValuesIterator = new Set().values()
			expect(kindOf(SetValuesIterator)).toBe('setiterator')
		})
		it('should work for Map Iterator', (): void => {
			const MapValuesIterator = new Map().values()
			expect(kindOf(MapValuesIterator)).toBe('mapiterator')
		})
		it('should work for Array Iterator', (): void => {
			const ArrayEntriesIterator = [].entries()
			expect(kindOf(ArrayEntriesIterator)).toBe('arrayiterator')
		})
		it('should work for String Iterator', (): void => {
			const StringCharIterator = ''[Symbol.iterator]()
			expect(kindOf(StringCharIterator)).toBe('stringiterator')
		})

		it('should work for Symbol', (): void => {
			expect(kindOf(Symbol('foo'))).toBe('symbol')
			expect(kindOf(Symbol.prototype)).toBe('symbol')
		})

		it('should work for Int8Array', (): void => {
			const int8array = new Int8Array()
			expect(kindOf(int8array)).toBe('int8array')
		})

		it('should work for Uint8Array', (): void => {
			const uint8array = new Uint8Array()
			expect(kindOf(uint8array)).toBe('uint8array')
		})

		it('should work for Uint8ClampedArray', (): void => {
			const uint8clampedarray = new Uint8ClampedArray()
			expect(kindOf(uint8clampedarray)).toBe('uint8clampedarray')
		})

		it('should work for Int16Array', (): void => {
			const int16array = new Int16Array()
			expect(kindOf(int16array)).toBe('int16array')
		})

		it('should work for Uint16Array', (): void => {
			const uint16array = new Uint16Array()
			expect(kindOf(uint16array)).toBe('uint16array')
		})

		it('should work for Int32Array', (): void => {
			const int32array = new Int32Array()
			expect(kindOf(int32array)).toBe('int32array')
		})

		it('should work for Uint32Array', (): void => {
			const uint32array = new Uint32Array()
			expect(kindOf(uint32array)).toBe('uint32array')
		})

		it('should work for Float32Array', (): void => {
			const float32array = new Float32Array()
			expect(kindOf(float32array)).toBe('float32array')
		})

		it('should work for Float64Array', (): void => {
			const float64array = new Float64Array()
			expect(kindOf(float64array)).toBe('float64array')
		})
	})
})
