import { deepClone } from '../../../libs/utils/deepClone'
import { hasProp } from '../../../libs/utils/hasProp'

describe('deepClone()', (): void => {
	it('should clone arrays', (): void => {
		const arr1 = ['alpha', 'beta', 'gamma']
		const arr2 = [1, 2, 3]

		expect(deepClone(deepClone(arr1))).toEqual(arr1)
		expect(deepClone(arr2)).toEqual(arr2)

		const source = [{ a: 0 }, { b: 1 }]
		const result = deepClone(source)

		expect(source).toEqual(result)
		expect(source[0]).not.toBe(result[0])

		const val = [0, 'a', {}, [{}], [function() {}], (): void => {}]
		expect(deepClone(val)).toEqual(val)
	})

	it('should deeply clone an array', (): void => {
		const source = [[{ a: 'b' }], [{ a: 'b' }]]
		const result = deepClone(source)

		expect(source).not.toBe(result)
		expect(source[0]).not.toBe(result[0])
		expect(source[1]).not.toBe(result[1])
		expect(source).toEqual(result)
	})

	it('should deeply clone object', (): void => {
		const source = { a: 'b', c: 'c' }
		const result = deepClone(source)

		result.c = 'd'

		expect(source).not.toEqual(result)
	})

	it('should deeply clone arrays', (): void => {
		const one = { a: 'b', c: 'c' }
		const arr1 = [one]
		const arr2 = deepClone(arr1)

		one.c = 'd'

		expect(arr1).not.toEqual(arr2)
	})

	it('should deeply clone Map', (): void => {
		const source = new Map([[1, 5]])
		const result = deepClone(source)

		source.set(2, 4)

		expect(Array.from(source)).not.toEqual(Array.from(result))
	})

	it('should deeply clone Set', (): void => {
		const source = new Set([2, 1, 3])
		const result = deepClone(source)

		source.add(8)

		expect(Array.from(source)).not.toEqual(Array.from(result))
	})

	it('should return primitives', (): void => {
		expect(deepClone(0)).toBe(0)
		expect(deepClone('foo')).toBe('foo')
	})

	it('should clone a regex', (): void => {
		expect(deepClone(/foo/g)).toEqual(/foo/g)
	})

	it('should clone objects', (): void => {
		const obj = { a: 1, b: 2, c: 3 }

		expect(deepClone(obj)).toEqual(obj)
	})

	it('should deeply clone objects', (): void => {
		const obj = {
			a: { a: 1, b: 2, c: 3 },
			b: { a: 1, b: 2, c: 3 },
			c: { a: 1, b: 2, c: 3 }
		}

		expect(deepClone(obj)).toEqual(obj)
	})

	it('should deep clone instances with instanceClone true', (): void => {
		class A {
			public x: any

			public y: any

			public z: any

			public constructor(x?: any, y?: any, z?: any) {
				this.x = x
				this.y = y
				this.z = z
			}
		}

		class B {
			public x: any

			public constructor(x?: any) {
				this.x = x
			}
		}

		const source = new A({ x: 11, y: 12, z: () => 'z' }, new B(2), 7)
		const result = deepClone(source, true)

		expect(source).toEqual(result)

		result.y.x = 1
		result.z = 2

		expect(source).not.toEqual(result)
		expect(source.z).not.toEqual(result.z)
		expect(source.y.x).not.toEqual(result.y.x)
	})

	it('should not deep clone instances', (): void => {
		class A {
			public x: any

			public y: any

			public z: any

			public constructor(x?: any, y?: any, z?: any) {
				this.x = x
				this.y = y
				this.z = z
			}
		}

		class B {
			public x: any

			public constructor(x?: any) {
				this.x = x
			}
		}

		const source = new A({ x: 11, y: 12, z: () => 'z' }, new B(2), 7)
		const result = deepClone(source)

		expect(source).toEqual(result)

		result.y.x = 1
		result.z = 2

		expect(source).not.toEqual(result)
		expect(source.z).not.toEqual(result.z)
		expect(source.y.x).not.toEqual(result.y.x)
	})

	it('should deep clone instances with instanceClone self defined', (): void => {
		class A {
			public x: any

			public y: any

			public z: any

			public constructor(x?: any, y?: any, z?: any) {
				this.x = x
				this.y = y
				this.z = z
			}
		}

		class B {
			public x: any

			public constructor(x?: any) {
				this.x = x
			}
		}

		const source = new A({ x: 11, y: 12, z: () => 'z' }, new B(2), 7)
		const result = deepClone(source, (val: any): any => {
			if (val instanceof A) {
				const res = new A()
				for (const key in val) {
					if (hasProp(res, key)) {
						res[key] = deepClone(val[key])
					}
				}
				return res
			}
			return deepClone(val)
		})

		expect(source).toEqual(result)

		result.y.x = 1
		result.z = 2

		expect(source).not.toEqual(result)
		expect(source.z).not.toEqual(result.z)
		expect(source.y.x).not.toBe(result.y.x)
	})
})
