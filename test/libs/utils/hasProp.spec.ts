import { hasProp } from '../../../libs/utils/hasProp'

describe('hasPath()', (): void => {
	const obj = { foo: 'foo' }

	it('should return true if property exist', (): void => {
		expect(hasProp(obj, 'foo')).toBe(true)
	})

	it('should return false if property does not exist', (): void => {
		expect(hasProp(obj, 'bar')).toBe(false)
	})
})
