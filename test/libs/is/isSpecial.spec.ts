import { isSpecial } from '../../../libs/is/isSpecial'

describe('isSpecial()', (): void => {
	it('should return true if value is of type date, regexp or symbol', (): void => {
		expect(isSpecial(new Date())).toBe(true)
		expect(isSpecial(/(?:)/)).toBe(true)
		expect(isSpecial(new RegExp(/(?:)/))).toBe(true)
		expect(isSpecial(Symbol('symbol'))).toBe(true)
	})

	it('should return false if value is not of type date, regexp or symbol', (): void => {
		expect(isSpecial([])).toBe(false)
		expect(isSpecial(new Array(42))).toBe(false)
		expect(isSpecial(true)).toBe(false)
		expect(isSpecial(new Boolean(false))).toBe(false)
		expect(isSpecial(new Function())).toBe(false)
		expect(isSpecial(() => {})).toBe(false)
		expect(isSpecial(null)).toBe(false)
		expect(isSpecial(42)).toBe(false)
		expect(isSpecial(new Number(42))).toBe(false)
		expect(isSpecial({})).toBe(false)
		expect(isSpecial(new Object())).toBe(false)
		expect(isSpecial('Hello World!')).toBe(false)
		expect(isSpecial(new String())).toBe(false)
		expect(isSpecial(undefined)).toBe(false)
	})
})
