import { isString } from '../../../libs/is/isString'

describe('isString()', (): void => {
	it('should return true if value is of type string', (): void => {
		expect(isString('Hello World!')).toBe(true)
		expect(isString(new String())).toBe(true)
	})

	it('should return false if value is not of type string', (): void => {
		expect(isString([])).toBe(false)
		expect(isString(new Array(42))).toBe(false)
		expect(isString(true)).toBe(false)
		expect(isString(new Boolean(false))).toBe(false)
		expect(isString(new Date())).toBe(false)
		expect(isString(new Function())).toBe(false)
		expect(isString(() => {})).toBe(false)
		expect(isString(null)).toBe(false)
		expect(isString(42)).toBe(false)
		expect(isString(new Number(42))).toBe(false)
		expect(isString({})).toBe(false)
		expect(isString(new Object())).toBe(false)
		expect(isString(/(?:)/)).toBe(false)
		expect(isString(new RegExp(/(?:)/))).toBe(false)
		expect(isString(Symbol('symbol'))).toBe(false)
		expect(isString(undefined)).toBe(false)
	})
})
