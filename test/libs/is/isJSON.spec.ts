import { isJSON } from '../../../libs/is/isJSON'

describe('isJSON()', (): void => {
	it('should return true if value is of type JSON', (): void => {
		expect(
			isJSON(
				JSON.stringify({
					foo: {
						bar: 'bar',
						faz: 'faz'
					}
				})
			)
		).toBe(true)
	})

	it('should return false if value is not of type JSON', (): void => {
		expect(isJSON([])).toBe(false)
		expect(isJSON(new Array(42))).toBe(false)
		expect(isJSON(true)).toBe(false)
		expect(isJSON(new Boolean(false))).toBe(false)
		expect(isJSON(new Date())).toBe(false)
		expect(isJSON(new Function())).toBe(false)
		expect(isJSON(() => {})).toBe(false)
		expect(isJSON(null)).toBe(false)
		expect(isJSON(42)).toBe(false)
		expect(isJSON(new Number(42))).toBe(false)
		expect(isJSON({})).toBe(false)
		expect(isJSON(new Object())).toBe(false)
		expect(isJSON(/(?:)/)).toBe(false)
		expect(isJSON(new RegExp(/(?:)/))).toBe(false)
		expect(isJSON('Hello World!')).toBe(false)
		expect(isJSON(new String())).toBe(false)
		expect(isJSON(Symbol('symbol'))).toBe(false)
		expect(isJSON(undefined)).toBe(false)
	})
})
