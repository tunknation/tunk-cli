import { isBoolean } from '../../../libs/is/isBoolean'

describe('isBoolean()', (): void => {
	it('should return true if value is of type boolean', (): void => {
		expect(isBoolean(true)).toBe(true)
		expect(isBoolean(new Boolean(false))).toBe(true)
	})

	it('should return false if value is not of type boolean', (): void => {
		expect(isBoolean([])).toBe(false)
		expect(isBoolean(new Array(42))).toBe(false)
		expect(isBoolean(new Date())).toBe(false)
		expect(isBoolean(() => {})).toBe(false)
		expect(isBoolean(new Function())).toBe(false)
		expect(isBoolean(42)).toBe(false)
		expect(isBoolean(new Number(42))).toBe(false)
		expect(isBoolean({})).toBe(false)
		expect(isBoolean(new Object())).toBe(false)
		expect(isBoolean(/(?:)/)).toBe(false)
		expect(isBoolean(new RegExp(/(?:)/))).toBe(false)
		expect(isBoolean('Hello World!')).toBe(false)
		expect(isBoolean(Symbol('symbol'))).toBe(false)
		expect(isBoolean(new String())).toBe(false)
	})
})
