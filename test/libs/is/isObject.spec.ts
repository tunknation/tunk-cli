import { isObject } from '../../../libs/is/isObject'

describe('isObject()', (): void => {
	it('should return true if value is of type object', (): void => {
		expect(isObject({})).toBe(true)
		expect(isObject(new Object())).toBe(true)
	})

	it('should return false if value is not of type object', (): void => {
		expect(isObject([])).toBe(false)
		expect(isObject(new Array(42))).toBe(false)
		expect(isObject(true)).toBe(false)
		expect(isObject(new Boolean(false))).toBe(false)
		expect(isObject(new Date())).toBe(false)
		expect(isObject(new Function())).toBe(false)
		expect(isObject(() => {})).toBe(false)
		expect(isObject(null)).toBe(false)
		expect(isObject(42)).toBe(false)
		expect(isObject(new Number(42))).toBe(false)
		expect(isObject(/(?:)/)).toBe(false)
		expect(isObject(new RegExp(/(?:)/))).toBe(false)
		expect(isObject('Hello World!')).toBe(false)
		expect(isObject(new String())).toBe(false)
		expect(isObject(Symbol('symbol'))).toBe(false)
		expect(isObject(undefined)).toBe(false)
	})
})
