import { isNumber } from '../../../libs/is/isNumber'

describe('isNumber()', (): void => {
	it('should return true if value is of type number', (): void => {
		expect(isNumber(42)).toBe(true)
		expect(isNumber(new Number(42))).toBe(true)
	})

	it('should return false if value is not of type number', (): void => {
		expect(isNumber([])).toBe(false)
		expect(isNumber(new Array(42))).toBe(false)
		expect(isNumber(true)).toBe(false)
		expect(isNumber(new Boolean(false))).toBe(false)
		expect(isNumber(new Date())).toBe(false)
		expect(isNumber(new Function())).toBe(false)
		expect(isNumber(() => {})).toBe(false)
		expect(isNumber(null)).toBe(false)
		expect(isNumber({})).toBe(false)
		expect(isNumber(new Object())).toBe(false)
		expect(isNumber(/(?:)/)).toBe(false)
		expect(isNumber(new RegExp(/(?:)/))).toBe(false)
		expect(isNumber('Hello World!')).toBe(false)
		expect(isNumber(new String())).toBe(false)
		expect(isNumber(Symbol('symbol'))).toBe(false)
		expect(isNumber(undefined)).toBe(false)
	})
})
