import { isRegExp } from '../../../libs/is/isRegExp'

describe('isRegExp()', (): void => {
	it('should return true if value is of type regexp', (): void => {
		expect(isRegExp(/(?:)/)).toBe(true)
		expect(isRegExp(new RegExp(/(?:)/))).toBe(true)
	})

	it('should return false if value is not of type regexp', (): void => {
		expect(isRegExp([])).toBe(false)
		expect(isRegExp(new Array(42))).toBe(false)
		expect(isRegExp(true)).toBe(false)
		expect(isRegExp(new Boolean(false))).toBe(false)
		expect(isRegExp(new Date())).toBe(false)
		expect(isRegExp(new Function())).toBe(false)
		expect(isRegExp(() => {})).toBe(false)
		expect(isRegExp(null)).toBe(false)
		expect(isRegExp(42)).toBe(false)
		expect(isRegExp(new Number(42))).toBe(false)
		expect(isRegExp({})).toBe(false)
		expect(isRegExp(new Object())).toBe(false)
		expect(isRegExp('Hello World!')).toBe(false)
		expect(isRegExp(new String())).toBe(false)
		expect(isRegExp(Symbol('symbol'))).toBe(false)
		expect(isRegExp(undefined)).toBe(false)
	})
})
