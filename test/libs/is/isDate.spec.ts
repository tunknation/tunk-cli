import { isDate } from '../../../libs/is/isDate'

describe('isDate()', (): void => {
	it('should return true if value is of type date', (): void => {
		expect(isDate(new Date())).toBe(true)
	})

	it('should return false if value is not of type date', (): void => {
		expect(isDate([])).toBe(false)
		expect(isDate(new Array(42))).toBe(false)
		expect(isDate(true)).toBe(false)
		expect(isDate(new Boolean(false))).toBe(false)
		expect(isDate(() => {})).toBe(false)
		expect(isDate(new Function())).toBe(false)
		expect(isDate(42)).toBe(false)
		expect(isDate(new Number(42))).toBe(false)
		expect(isDate({})).toBe(false)
		expect(isDate(new Object())).toBe(false)
		expect(isDate(/(?:)/)).toBe(false)
		expect(isDate(new RegExp(/(?:)/))).toBe(false)
		expect(isDate('Hello World!')).toBe(false)
		expect(isDate(Symbol('symbol'))).toBe(false)
		expect(isDate(new String())).toBe(false)
	})
})
