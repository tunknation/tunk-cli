import { isSymbol } from '../../../libs/is/isSymbol'

describe('isSymbol()', (): void => {
	it('should return true if value is of type symbol', (): void => {
		expect(isSymbol(Symbol('symbol'))).toBe(true)
	})

	it('should return false if value is not of type symbol', (): void => {
		expect(isSymbol([])).toBe(false)
		expect(isSymbol(new Array(42))).toBe(false)
		expect(isSymbol(true)).toBe(false)
		expect(isSymbol(new Boolean(false))).toBe(false)
		expect(isSymbol(new Date())).toBe(false)
		expect(isSymbol(new Function())).toBe(false)
		expect(isSymbol(() => {})).toBe(false)
		expect(isSymbol(null)).toBe(false)
		expect(isSymbol(42)).toBe(false)
		expect(isSymbol(new Number(42))).toBe(false)
		expect(isSymbol({})).toBe(false)
		expect(isSymbol(new Object())).toBe(false)
		expect(isSymbol(/(?:)/)).toBe(false)
		expect(isSymbol(new RegExp(/(?:)/))).toBe(false)
		expect(isSymbol('Hello World!')).toBe(false)
		expect(isSymbol(new String())).toBe(false)
		expect(isSymbol(undefined)).toBe(false)
	})
})
