import { isNullOrUndefined } from '../../../libs/is/isNullOrUndefined'

describe('isNullOrUndefined()', (): void => {
	it('should return true if value is of type null or undefined', (): void => {
		expect(isNullOrUndefined(null)).toBe(true)
		expect(isNullOrUndefined(undefined)).toBe(true)
	})

	it('should return false if value is not of type null or undefined', (): void => {
		expect(isNullOrUndefined([])).toBe(false)
		expect(isNullOrUndefined(new Array(42))).toBe(false)
		expect(isNullOrUndefined(true)).toBe(false)
		expect(isNullOrUndefined(new Boolean(false))).toBe(false)
		expect(isNullOrUndefined(new Date())).toBe(false)
		expect(isNullOrUndefined(new Function())).toBe(false)
		expect(isNullOrUndefined(() => {})).toBe(false)
		expect(isNullOrUndefined(42)).toBe(false)
		expect(isNullOrUndefined(new Number(42))).toBe(false)
		expect(isNullOrUndefined({})).toBe(false)
		expect(isNullOrUndefined(new Object())).toBe(false)
		expect(isNullOrUndefined(/(?:)/)).toBe(false)
		expect(isNullOrUndefined(new RegExp(/(?:)/))).toBe(false)
		expect(isNullOrUndefined('Hello World!')).toBe(false)
		expect(isNullOrUndefined(Symbol('symbol'))).toBe(false)
		expect(isNullOrUndefined(new String())).toBe(false)
	})
})
