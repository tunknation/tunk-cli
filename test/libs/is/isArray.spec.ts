import { isArray } from '../../../libs/is/isArray'

describe('isArray()', (): void => {
	it('should return true if value is of type array', (): void => {
		expect(isArray([])).toBe(true)
		expect(isArray(new Array(42))).toBe(true)
	})

	it('should return false if value is not of type array', (): void => {
		expect(isArray(true)).toBe(false)
		expect(isArray(new Boolean(false))).toBe(false)
		expect(isArray(new Date())).toBe(false)
		expect(isArray(new Function())).toBe(false)
		expect(isArray(() => {})).toBe(false)
		expect(isArray(null)).toBe(false)
		expect(isArray(42)).toBe(false)
		expect(isArray(new Number(42))).toBe(false)
		expect(isArray({})).toBe(false)
		expect(isArray(new Object())).toBe(false)
		expect(isArray(/(?:)/)).toBe(false)
		expect(isArray(new RegExp(/(?:)/))).toBe(false)
		expect(isArray('Hello World!')).toBe(false)
		expect(isArray(new String())).toBe(false)
		expect(isArray(Symbol('symbol'))).toBe(false)
		expect(isArray(undefined)).toBe(false)
	})
})
