import { isPrimitive } from '../../../libs/is/isPrimitive'

describe('isPrimitive()', (): void => {
	it('should return true if value is of type primitive', (): void => {
		expect(isPrimitive(true)).toBe(true)
		expect(isPrimitive(new Boolean(false))).toBe(true)
		expect(isPrimitive(null)).toBe(true)
		expect(isPrimitive(42)).toBe(true)
		expect(isPrimitive(new Number(42))).toBe(true)
		expect(isPrimitive('Hello World!')).toBe(true)
		expect(isPrimitive(new String())).toBe(true)
		expect(isPrimitive(Symbol('symbol'))).toBe(true)
		expect(isPrimitive(undefined)).toBe(true)
	})

	it('should return false if value is not of type primitive', (): void => {
		expect(isPrimitive([])).toBe(false)
		expect(isPrimitive(new Array(42))).toBe(false)
		expect(isPrimitive(new Date())).toBe(false)
		expect(isPrimitive(new Function())).toBe(false)
		expect(isPrimitive(() => {})).toBe(false)
		expect(isPrimitive({})).toBe(false)
		expect(isPrimitive(new Object())).toBe(false)
		expect(isPrimitive(/(?:)/)).toBe(false)
		expect(isPrimitive(new RegExp(/(?:)/))).toBe(false)
	})
})
