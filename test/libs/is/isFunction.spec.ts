import { isFunction } from '../../../libs/is/isFunction'

describe('isFunction()', (): void => {
	it('should return true if value is of type function', (): void => {
		expect(isFunction(() => {})).toBe(true)
	})

	it('should return false if value is not of type function', (): void => {
		expect(isFunction([])).toBe(false)
		expect(isFunction(new Array(42))).toBe(false)
		expect(isFunction(true)).toBe(false)
		expect(isFunction(new Boolean(false))).toBe(false)
		expect(isFunction(new Date())).toBe(false)
		expect(isFunction(null)).toBe(false)
		expect(isFunction(42)).toBe(false)
		expect(isFunction(new Number(42))).toBe(false)
		expect(isFunction({})).toBe(false)
		expect(isFunction(new Object())).toBe(false)
		expect(isFunction(/(?:)/)).toBe(false)
		expect(isFunction(new RegExp(/(?:)/))).toBe(false)
		expect(isFunction('Hello World!')).toBe(false)
		expect(isFunction(new String())).toBe(false)
		expect(isFunction(Symbol('symbol'))).toBe(false)
		expect(isFunction(undefined)).toBe(false)
	})
})
