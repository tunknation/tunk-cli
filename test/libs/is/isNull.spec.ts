import { isNull } from '../../../libs/is/isNull'

describe('isNull()', (): void => {
	it('should return true if value is of type null', (): void => {
		expect(isNull(null)).toBe(true)
	})

	it('should return false if value is not of type null', (): void => {
		expect(isNull([])).toBe(false)
		expect(isNull(new Array(42))).toBe(false)
		expect(isNull(true)).toBe(false)
		expect(isNull(new Boolean(false))).toBe(false)
		expect(isNull(new Date())).toBe(false)
		expect(isNull(new Function())).toBe(false)
		expect(isNull(() => {})).toBe(false)
		expect(isNull(42)).toBe(false)
		expect(isNull(new Number(42))).toBe(false)
		expect(isNull({})).toBe(false)
		expect(isNull(new Object())).toBe(false)
		expect(isNull(/(?:)/)).toBe(false)
		expect(isNull(new RegExp(/(?:)/))).toBe(false)
		expect(isNull('Hello World!')).toBe(false)
		expect(isNull(new String())).toBe(false)
		expect(isNull(Symbol('symbol'))).toBe(false)
		expect(isNull(undefined)).toBe(false)
	})
})
