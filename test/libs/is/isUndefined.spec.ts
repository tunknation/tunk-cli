import { isUndefined } from '../../../libs/is/isUndefined'

describe('isUndefined()', (): void => {
	it('should return true if value is of type undefined', (): void => {
		expect(isUndefined(undefined)).toBe(true)
	})

	it('should return false if value is not of type undefined', (): void => {
		expect(isUndefined([])).toBe(false)
		expect(isUndefined(new Array(42))).toBe(false)
		expect(isUndefined(true)).toBe(false)
		expect(isUndefined(new Boolean(false))).toBe(false)
		expect(isUndefined(new Date())).toBe(false)
		expect(isUndefined(new Function())).toBe(false)
		expect(isUndefined(() => {})).toBe(false)
		expect(isUndefined(null)).toBe(false)
		expect(isUndefined(42)).toBe(false)
		expect(isUndefined(new Number(42))).toBe(false)
		expect(isUndefined({})).toBe(false)
		expect(isUndefined(new Object())).toBe(false)
		expect(isUndefined(/(?:)/)).toBe(false)
		expect(isUndefined(new RegExp(/(?:)/))).toBe(false)
		expect(isUndefined('Hello World!')).toBe(false)
		expect(isUndefined(new String())).toBe(false)
		expect(isUndefined(Symbol('symbol'))).toBe(false)
	})
})
